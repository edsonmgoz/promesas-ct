let promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise1");
    }, 500);
})

let promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise2");
    }, 1000);
})

let promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise3");
    }, 1500);
})

let promise4 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise4");
    }, 2000);
})

let promiseArr = [promise1, promise2, promise3, promise4];
Promise.all(promiseArr)
    .then(response => {
        console.log("OK => ", response);
    })
    .catch(err => {
        console.log("ERR => ", err);
    })