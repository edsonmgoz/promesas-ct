let promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise1");
    }, 10000);
})

let promise2 = new Promise((resolve, reject) => {
    setTimeout(() => {
        reject("Promise2");
    }, 1000);
})

let promise3 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise3");
    }, 1500);
})

let promise4 = new Promise((resolve, reject) => {
    setTimeout(() => {
        resolve("Promise4");
    }, 2000);
})

let promiseArr = [promise1, promise2, promise3, promise4];
// race ejecuta todas las promesas y termina la ejecucion cuando la primera de todas termine
Promise.race(promiseArr)
    .then(response => {
        console.log("OK => ", response);
    })
    .catch(err => {
        console.log("ERR => ", err);
    })