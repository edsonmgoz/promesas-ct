const fetch = require('node-fetch');

function getUserByUsername(username) {
    const url = `https://api.github.com/users/${username}`;

    return new Promise((resolve, reject) => {
        fetch(url)
            .then(response => {
                return response.json();
            })
            .then(jsonResponse => {
                resolve(jsonResponse)
            })
            .catch(err => {
                reject(err)
            });
    });
}

let response = getUserByUsername('edsonmgoz');
response
    .then(githubResponse => {
        console.log(githubResponse.name);
    })
    .catch(err => {
        console.log(err);
    })
// console.log("response =>", response);
