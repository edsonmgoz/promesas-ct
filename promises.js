let promise1 = new Promise((resolve, reject) => {
    // Aqui colocar toda la logica asincrona...
    // El estado por default que tiene una promesa cuando es creada es pendiente (pending)
    // una promesa mientras no haya sido ejecutada con then esta en pendiente
    // una vez que la promesa es ejecutada puede tener 2 estados, una es resuelta (resolve)
    // o fallida (reject)...

    setTimeout(() => {
        reject("FAIL");
    }, 5000);

    // resolve("OK");
    // reject("FAIL");
});

promise1
    .then(response => {
        console.log(response);
    })
    .catch(err => {
        console.log(err);
    })
    .finally(() => {
        // Finally funciona independientemente de q se ejecute then o catch
        console.log("Finally");
    })